import logo from "./logo.svg";
import "./App.css";
import Products from "./components/Products/products";
import { useState } from "react";
import Wishlist from "./components/Wishlist/wishlist";
import "./assets/icomoon/style.css";
import SimpleSlider from "./components/SimpleSlider";

function App() {
  var [open, setOpen] = useState(false);
  const products_id = [
    "almofada-pelucia",
    "bola-com-abertura-para-racao",
    "cama-pequena",
    "arranhador-2-andares",
    "urso-teddy-bear",
    "pelucias-sortidas",
  ];
  const products_name = [
    "Almofada - Pelúcia",
    "Bola Com Abertura Para Ração",
    "Cama - Pequena",
    "Arranhador - 2 Andares",
    "Urso - Teddy Bear",
    "Pelúcias - Sortidas",
  ];
  const products_price = ["30,00", "10,00", "25,20", "60,00", "32,00", "15,90"];
  return (
    <div className="App">
      <header>
        <nav id="top-bar">
          <div class="group-1">
            <span id="logo-type" class="icon-github"></span>
          </div>
          <div class="group-2">
            <span class="icon-pesquisa"></span>
          </div>
          <div class="group-3">
            <button
              class="wishlist-btn"
              onClick={function () {
                setOpen(open === true ? false : true);
              }}
            >
              <span class="icon-favorite"></span>
            </button>
            <span class="icon-64572"></span>
          <span class="icon-basket"></span>
          </div>
        </nav>
        <nav id="bottom-bar">
          <ul>
            <li><a href="#">Lançamentos</a></li>
            <li><a href="#">Gatos</a></li>
            <li><a href="#">Humanos</a></li>
            <li><a href="#">Promoções</a></li>
          </ul>
        </nav>
      </header>
      <dialog open={open}>
        <button
          class="wishlist-btn"
          onClick={function () {
            setOpen(open === true ? false : true);
          }}
        >
          fechar
        </button>
        <Wishlist
          id={products_id}
          name={products_name}
          price={products_price}
        />
      </dialog>
      <SimpleSlider id="slider"/>

      <main>
      <h2>Lançamentos</h2>
        <section id="product-section" class="grid">
          <Products
            id={products_id}
            name={products_name}
            price={products_price}
          />
        </section>
      </main>

    </div>
  );
}

export default App;
