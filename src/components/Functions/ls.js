function addClassSaved(id, local) {
  if (localStorage.getItem(id) == null) {
    document.querySelector(local + " ." + id).classList.remove("saved");
  } else {
    document.querySelector(local + " ." + id).classList.add("saved");
  }
}

export default addClassSaved;
