import Products from "../Products/products";
import "./wishlist.css";

function Wishlist(props) {
  var wish_id = [];
  var wish_names = [];
  var wish_prices = [];
  for (var i = 0; i < props.name.length; i++) {
    if (localStorage.getItem(props.id[i])) {
      wish_id.push(props.id[i]);
      wish_names.push(props.name[i]);
      wish_prices.push(props.price[i]);
    }
  }
  return (
    <section id="wishlist-section" class="grid">
      <Products id={wish_id} name={wish_names} price={wish_prices} />
    </section>
  );
}
export default Wishlist;
