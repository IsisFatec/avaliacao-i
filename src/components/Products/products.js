import "./products.css"
import addClassSaved from "../Functions/ls";

function Products(props) {
  const product_qnt = props.name.length;
  let rows = [];
  for (let i = 0; i < product_qnt; i++) {
    rows.push(
      <div id={props.id[i]} class={"product-card grid " + props.id[i]}>
        <div class="product-img"></div>
        <div class="product-info">
          <h3>{props.name[i]}</h3>
          <div class="product-buttons">
            <button
              class="add_wish_btn"
              onClick={() =>
                saveToWishlist(props.id[i], props.name[i], props.price[i])
              }
            >
              <span class="icon-favorite"></span>
            </button>
            <button class="buy_btn">R$ {props.price[i]}</button>
          </div>
        </div>
      </div>
    );
  }

  return <> {rows}</>;
}

function saveToWishlist(id, name, price) {
  let data = {
    id: id,
    name: name,
    price: price,
  };
  if (localStorage.getItem(id)) {
    localStorage.removeItem(id);

    // Eliminar o produto da wishlist imediatamente se a própria estiver aberta
    if ((id != null) & (document.querySelector("dialog ." + id) != null)) {
      document.querySelector("dialog ." + id).classList.add("invisible");
    }
  } else {
    localStorage.setItem(id, JSON.stringify(data));
  }
  addClassSaved(id, "main")
}

export default Products;
