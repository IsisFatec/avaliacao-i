import React, { Component } from "react";
import Slider from "react-slick";
import "./SimpleSlider.css"

export default class SimpleSlider extends Component {
  render() {
    const settings = {
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 200,
    };
    return (
      <div id="slider">
        <div class="img1 div-img"/>
        <div class="img2 div-img"/>
      </div>
    );
  }
}